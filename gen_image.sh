#!/bin/bash

var=`fswebcam -s 3 -D 2 -r 1920x1080 --no-banner  /var/www/html/images/image.jpg 2>&1`
echo $var

if [[ $var =~ "Unable to find a compatible palette format" ]]
then
	idpair=`lsusb |sed -ne '/Logi/ s/Bus \(...\) Device \(...\):.*/\1\/\2/p'`
	device=/dev/bus/usb/$idpair
	echo Reset usb $device
	var2=`sudo /usr/local/bin/usbreset $device 2>&1`
	
	echo $var2
fi 

sudo chmod a=wrx /var/www/html/images/image.jpg

#echo Take picture 
#fswebcam -s 3 -D 2 -r 1920x1080 --no-banner  /var/www/html/images/image.jpg 2>&1
#sudo chmod a=wrx /var/www/html/images/image.jpg
#fswebcam -s 3 -D 2 -r 320x240 -s Brightness=200 -s Contrast=70 bild.jpg

